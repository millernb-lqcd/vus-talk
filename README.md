# $`\vert V_{us} \vert`$ from the Lattice

Talk on methods for determining $`\vert V_{us} \vert`$ from lattice QCD.

Link to slides:

- https://millernb-lqcd.gitlab.io/vus-talk/presentation.pdf

## Abstract

Since the 1950s physicists have known that strangeness is not a conserved quantum number in certain particle interactions. Today we know that flavor is specifically not conserved under the weak interaction, a feature of the Standard Model encoded in in the Cabibbo-Kobayashi-Maskawa matrix. The Standard Model predicts this matrix to be unitary, from which one derives the so called "top-row unitarity" condition, which would constrain the matrix elements $`\vert V_{ud} \vert`$, $`\vert V_{us} \vert`$, and $`\vert V_{ub} \vert`$ if true. In this talk, I will review the different lattice methods for obtaining an estimate of the CKM matrix element $`\vert V_{us} \vert`$, which is particularly amenable to the lattice. I will summarize our calculation of the ratio of the pseudoscalar decay constants $`F_K/F_\pi`$, from which we obtained an estimate of $`\vert V_{us} \vert`$, as well as our on-going work on hyperon observables, from which one could obtain an orthogonal estimate of $`\vert V_{us} \vert`$. 