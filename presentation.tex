\documentclass[usenames,dvipsnames,aspectratio=169]{beamer}
\title{$\vert V_{us} \vert$ from the lattice}

\author[shortname]{
	\texorpdfstring{\textcolor{ProcessBlue}{Nolan Miller}}{} \and 
	E.~Berkowitz \and
	G.~Bradley \and
	D.~A~Brantley \and
	H.~Monge-Camacho \and
	C.~C.~Chang \and
	M.~A.~Clark \and
	A.~S~Gambhir \and
	B.~H\"orz \and
	D.~Howarth \and
	B.~Jo\'{o} \and
	C.~K\"orber \and
	T.~Kurth \and
	M.~Lazarow \and
	C.~J.~Monahan \and
	A.~Nicholson \and
	K.~Orginos \and
	E.~Rinaldi \and
	P.~Vranas \and
	A.~Walker-Loud \and
	\emph{others}
}
					  
\date{November 23, 2021}

\titlegraphic{
	\includegraphics[height=2cm]{figs/callat_logo.png}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/doe_sc_logo.pdf}\hspace*{1cm}~%
	\includegraphics[height=2cm]{figs/unc_logo.png} %unc_logo
}

\usepackage{braket}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{slashed}
\usepackage{cancel}

\usetheme{default}
\usecolortheme{dove}
\setbeamercolor{frametitle}{fg=RoyalPurple,bg=Orchid!20}

\setbeamertemplate{navigation symbols}{%
	\usebeamerfont{footline}%
	\usebeamercolor[fg]{footline}%
	\hspace{1em}%
	\insertframenumber
}
\addtobeamertemplate{frametitle}{
   \let\insertframesubtitle\insertsectionhead}{}
\setbeamertemplate{background}
{\includegraphics[width=\paperwidth,keepaspectratio]{figs/background.jpg}}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}
	\frametitle{Outline}
	\tableofcontents
\end{frame}



\section{Background}
\begin{frame}
	\begin{center}
		\Huge Background
	\end{center}	
\end{frame}

\begin{frame}{\secname}
	\frametitle{Flavor conservation?}
	\framesubtitle{The Cabibbo-Kobayashi-Maskawa matrix}
	\begin{columns}
		\begin{column}{0.5\textwidth}
		{\color{ProcessBlue} Question:} Are the quark eigenstates of the weak and strong interaction the same?
		\begin{equation} \nonumber
		q_\text{weak} = V q_\text{strong}
		\end{equation}
		{\color{ProcessBlue} Answer:} No!
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=0.7\textwidth]{figs/kaon_box_diagram.png}
				%\caption{[Wikipedia]}
			\end{figure}
		\end{column}
	\end{columns}
	\vfill
	\begin{equation}
	\begin{bmatrix}
	|V_{ud}| & |V_{us}| & |V_{ub}| \\
	|V_{cd}| & |V_{cs}| & |V_{cb}| \\
	|V_{td}| & |V_{ts}| & |V_{tb}|
	\end{bmatrix} = \begin{bmatrix}
	0.97446(10) & 0.22452(44) & 0.00365(12) \\
	0.22438(44) & 0.97359(11) & 0.04214(76) \\
	0.00896(24) & 0.04133(74) & 0.99910(03)
	\end{bmatrix} \nonumber
	\end{equation}
\end{frame}


\begin{frame}
	\frametitle{Top-row CKM unitarity}
	\begin{columns}
		\begin{column}{0.55\textwidth}
			Standard Model predicts $V^\dagger V= 1$. 

			\vspace{\baselineskip}
			We verify:
			\begin{equation*}
			\underbrace{|V_{ud}|^2}_{\substack{\text{Known from} \\ \text{experiments}}} +
			\underbrace{|V_{us}|^2}_{\substack{\text{Accessible} \\ \text{by lattice}}}  +
			\underbrace{|V_{ub}|^2}_{\substack{\text{Relatively} \\ \text{small}}} 
			= 1
			\end{equation*}
		\end{column}
		\begin{column}{0.45\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{figs/pdg_ckm_global.png}
				\caption*{\qquad [PDG (2020)]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Example: $V_{ud}$ from pion decay ($\pi \rightarrow l \, \overline{\nu}_l$)}
	Write down the {\color{ProcessBlue} transition matrix element} 
	\newline
	\begin{columns}
		\begin{column}{0.3\textwidth}
		\hspace*{1cm}\includegraphics[width=\textwidth]{figs/pion_decay.png}
		\end{column}
		\begin{column}{0.7\textwidth}
		\begin{equation*}
			= \frac{G_F}{\sqrt{2}} V_{ud} 
			\underbrace{\color{JungleGreen} \braket{0 | \overline{d} \gamma_\mu \gamma_5 u | \pi(p)}}_{i p_\mu {\color{RubineRed} F_\pi}}
			l \gamma^\mu (1 - \gamma^5) \overline{\nu}_l
		\end{equation*}
		\end{column}
	\end{columns}
	\vfill
	Apply Fermi's golden rule
	\begin{align*}
		&d\, \Gamma (\pi \rightarrow l \, \overline{\nu}_l) \propto \braket{| {\color{ProcessBlue} T_{fi}}|^2} d\, \Phi \\
		&\qquad \implies \Gamma (\pi \rightarrow l \, \overline{\nu}_l) 
		= (\text{stuff}) \times |V_{ud}|^2 {\color{RubineRed} F}^2_{\color{RubineRed} \pi}
	\end{align*}
\end{frame}


\begin{frame}
	\frametitle{Experimental determination of $|V_{ud}|$}
	\begin{figure}
		\includegraphics[width=0.65\textwidth]{figs/vud_error.png}
		\caption*{[Hardy \& Towner (2018); \href{https://arxiv.org/abs/1807.01146}{arXiv:1807.01146}]}
	\end{figure}
\end{frame}


\begin{frame}

	\frametitle{Experimental determination of $|V_{us}|$?}

	Experimental results are less precise without lattice QCD input
	\begin{itemize}
		\item Leptonic/semi-leptonic $K$ decays: requires LQCD estimate of $F_K/F_\pi$ or $f^+(0)$
		\item Hyperon decays: requires estimate of axial charge, vector charge, and other form factors; {\color{ProcessBlue} new results from LHCb could make this competitive}
		\item $\tau$ decays (eg, $\tau^- \rightarrow \pi^- \nu_\tau$): LQCD not required, but there are theory problems
	\end{itemize}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/vus_sources.png}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{Why use $F_K/F_\pi$ for $\vert V_{us}\vert$?}
	
	\begin{columns}
		\begin{column}{0.55\textwidth}
		$F_K / F_\pi$ is a \emph{gold-plated} quantity, serving as an important benchmark for testing lattice QCD actions
		\begin{itemize}
			\item dimensionless $\implies$ scale-setting unnecessary
			\item $F_K, F_\pi$ correlated $\implies$ high (sub-percent) precision
			\item mesonic, not baryonic $\implies$ no signal-to-noise problems
			\item chiral expansion known to $O(m_\pi^4)$ (NNLO) $\implies$ limited by statistics
		\end{itemize}
		\end{column}
		\begin{column}{0.45\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/vus_vs_vud.png}
				\caption*{[FLAG (2021); \href{https://arxiv.org/abs/2111.09849}{arXiv:2111.09849}]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Brief overview of Lattice QCD}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/sea_quarks_feynman.png}
				\caption*{[Randle-Conde (2014)]}
			\end{figure}
		\end{column}
		\begin{column}{0.6\textwidth}
			Lattice QCD is a non-pertubative approach to QCD
			\begin{align*}
				\langle O \rangle &= \frac{1}{Z_0} \int \mathcal{D}[q, \overline q] \mathcal{D}[A] \,  e^{iS_{\text{QCD}}[q, \overline q, A]} O[q, \overline q, A]\\
				&\rightarrow \langle O \rangle \approx \frac{1}{N}\sum_{q_n, \overline q_n, U_n} O[q_n, \overline q_n, U_n]
			\end{align*}
			s.t. $P(q_n, \overline q_n, U_n) \sim e^{-S_\text{LQCD} [q_n, \overline q_n, U_n]}$
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Measuring observables on the lattice (1/2)}
	\begin{columns}
	\begin{column}{0.45\textwidth}
	Generate correlator on lattice:
	\begin{equation}
		\underbrace{\braket{O(t)}}_{\substack{\text{Expectation} \\ \text{Value}}}
		\rightarrow
		\underbrace{\braket{\Omega | O(t) O^\dagger(0) | \Omega}}_{\substack{\text{Correlation} \\ \text{Function}}} 
		\nonumber
	\end{equation}
	Spectrally decompose:
	\begin{equation}
		C(t) = \braket{\Omega | O(t) O^\dagger(0) | \Omega} \approx \sum_n A_n e^{-E_n t} \nonumber
	\end{equation}
	From the correlator, we can determine the mass $E_0$ of the particle created by $O^\dagger$
	\end{column}
	\begin{column}{0.65\textwidth}
	\begin{figure}
		\includegraphics[width=\textwidth]{./figs/delta_a09m350_meff.pdf}
	\end{figure}
	\begin{equation*}
		m_\text{eff}(t) = \log \left[ \frac{C(t)}{C(t+1)} \right] \approx E_0
	\end{equation*}
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Measuring observables on the lattice (2/2)}
	\begin{columns}
		\begin{column}{0.25\textwidth}
			Convert to physical units:
			\begin{align*} \nonumber
			(aM)^\text{latt} &=  0.6617(65) \\
			\rightarrow M^\text{phys} &= \frac{(a M)^\text{latt}}{a}  \\
			&\approx 1400 \text{ MeV} 	
			\end{align*}	
			Snag: one does not generally know the lattice spacing \emph{a priori}
			\begin{equation*}
				a = \frac{(a M)^\text{latt}}{M^\text{phys}}
			\end{equation*}
		\end{column}
		\begin{column}{0.75\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/delta_a09m350_stability.pdf}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Scale setting with $w_0$ \& $M_\Omega$ (1/2)}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{./figs/symanzik_flow.pdf}
				\caption*{[BMW (2012); \href{https://arxiv.org/abs/1203.4469}{arXiv:1203.4469}]}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			Introduce a scale to convert dimensionless quantities. The observable should:
			\begin{itemize}
				\item be easy to generate on the lattice
				\item be precise
				\item have small systematics
				\item have little quark mass dependence
			\end{itemize}
			\vspace{\baselineskip}
			Candidate: scale {\color{ProcessBlue} $w_0$} defined by
			\begin{align*}
				W(t)& = t \frac{d}{dt} \Big[ t^2 \braket{E(t)} \Big] \\
				W(t)& \Big\vert_{t={\color{ProcessBlue} w_0}^2} = 0.3
			\end{align*}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{Scale setting with $w_0$ \& $M_\Omega$ (2/2)}
	\begin{columns}
	\begin{column}{0.4\textwidth}
		To determine $w_0^*$:
		\begin{itemize}
			\item Generate the dimensionless quantity $w_0/a$ per ensemble
			\item Extrapolate $w_0 M_\Omega = (w_0/a)(a M_\Omega)$
			\item Calculate $w_0^* = (w_0 M_\Omega)^*/M_\Omega^*$
		\end{itemize}
		\vspace{\baselineskip}
		Thus:
		\begin{align*}
			M^\text{phys} &= \frac{w_0/a}{w_0^*} ( a M)^\text{latt} \\
			&= 1486(13) \text{ MeV}
		\end{align*}
	\end{column}
	\begin{column}{0.6\textwidth}
		\begin{figure}
			\includegraphics[width=\textwidth]{./figs/w0_mO_vs_l_xpt_n3lo_FV_F.pdf}
			\caption*{[N.M./CalLat (2020); \href{https://arxiv.org/abs/2011.12166}{arXiv:2011.12166}]}
		\end{figure}
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Lattice QCD \& effective field theory}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{./figs/delta_a09m350_meff.pdf}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{./figs/mn_vs_epspi.pdf}
				%\caption*{Include extrapolation of a mass}
			\end{figure}
		\end{column}
	\end{columns}
	\vfill
	Augment lattice QCD calculations with EFT
	\begin{itemize}
		\item Lattice observables not generated at physical point
		\item Some ensembles are cheaper to generate than others
	\end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\Huge $\vert V_{us} \vert$ from $F_K/F_\pi$
	\end{center}	
\end{frame}


\section{$\vert V_{us} \vert$ from $F_K/F_\pi$}

\begin{frame}{$\vert V_{us} \vert$ from $F_K/F_\pi$}
	Per Marciano:
	\begin{equation} \notag
	\frac{\Gamma(K \rightarrow l \, \overline{\nu}_l)}{\Gamma(\pi \rightarrow l \, \overline{\nu}_l)} =
	\left({\color{RubineRed}\frac{F_K}{F_\pi}} \right)^2 \frac{|V_{us}|^2}{|V_{ud}|^2} \frac{m_K (1 - m_l^2/ m_K^2)^2}{m_\pi (1 - m_l^2/ m_\pi^2)^2} 
	\Big[1 + {\color{JungleGreen} \overbrace{\delta_\text{EM} + \delta_\text{SU(2)}}^{\text{QED/isospin}}} \Big]
	\end{equation}
	with decay constant definitions
	\begin{equation*}
	\braket{0 | \overline{d} \gamma_\mu \gamma_5 u | \pi(p)}= i p_\mu {\color{RubineRed}F_\pi}
	\qquad
	\braket{0 | \overline{s} \gamma_\mu \gamma_5 u | K(p)} = i p_\mu {\color{RubineRed}F_K}
	\end{equation*}

\end{frame}


\begin{frame}
	\frametitle{$F_K/F_\pi$ Models}
	Goal: Determine LECs $\implies$ extrapolate to physical point
	\begin{align*}
	\left(\frac{F_K}{F_\pi}\right)_\text{lattice} &= 1 
	+ {\color{JungleGreen} \delta \left(\frac{F_K}{F_\pi}\right)_\text{NLO}} 
	+ {\color{ProcessBlue} \delta\left(\frac{F_K}{F_\pi}\right)_\text{NNLO} }
	+ 	\delta\left(\frac{F_K}{F_\pi}\right)_\text{NNNLO} \\
	& \phantom{=}
	+ \delta \left(\frac{F_K}{F_\pi}\right)_\text{FV} 
	+ {\color{RubineRed} \delta \left(\frac{F_K}{F_\pi}\right)_\mu}
	+ {\color{RubineRed}  \delta \left(\frac{F_K}{F_\pi}\right)_{\Lambda_\chi}}
	+ {\color{Mulberry} \delta \left(\frac{F_K}{F_\pi}\right)_{\alpha_S}}
	\end{align*}

	\begin{columns}[T]
	\begin{column}{0.58\textwidth}
		Model choices:
		\begin{enumerate}
			\item {\color{JungleGreen} at NLO: ratio or taylor-expand}
			\item {\color{ProcessBlue} at NNLO: full $\chi$PT or counterterms only}
			\item {\color{RubineRed} $\mu^2 = \Lambda^2_\chi = 4 \pi \left\{F^2_\pi, F^2_K , F_\pi F_K\right\}$}
			\item {\color{Mulberry} include $\alpha_S$ term or not}
		\end{enumerate}
	\end{column}
	\begin{column}{0.42\textwidth}
		Input:
		\begin{itemize}
			\item $F_K$ and $F_\pi$
			\item $m_K$, $m_\pi$, and $m_\eta$
			\item lattice spacing
		\end{itemize}
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Example: NLO model}
	NLO expression ($\epsilon_p = m_p/\Lambda_\chi$): 
	\begin{align*}
		\left(\frac{F_K}{F_\pi}\right)_\text{$\chi$PT} &\approx 1 & \text{(LO)} \\
		&\phantom{=}
		+\frac{5}{8} \epsilon^2_\pi \log \epsilon^2_\pi
		-\frac{1}{4} \epsilon^2_K \log \epsilon^2_K
		-\frac{3}{8} \epsilon^2_\eta \log \epsilon^2_\eta
		\\
		&\phantom{=}
		+4 (4\pi)^2\underbrace{\color{magenta} \left( \epsilon^2_K - \epsilon^2_\pi \right)}_\text{SU(3) flavor} \underbrace{\color{red} L_5}_\text{LEC} & \text{(NLO)}
	\end{align*}
	\begin{columns}[T]
		\begin{column}{0.5\textwidth}
			NNLO corrections:
			\begin{itemize}
				\item $\mathcal{O}(m_p^4)$ counterterms from meson masses
				\item $\mathcal{O}(a^2)$ discretization errors
				\item loop-corrections
			\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			NNNLO corrections:
			\begin{itemize}
				\item $\mathcal{O}(m^6)$ counterterms from meson masses
				\item $\mathcal{O}(a^4)$ discretization errors
			\end{itemize}	
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Lattice details}

	\begin{columns}
		\begin{column}{0.55\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{./figs/ensembles_data.png}
				%\caption*{}
			\end{figure}
		\end{column}

		\begin{column}{0.45\textwidth}
			\begin{table}[]
				\begin{tabular}{|l|l|}
				\hline
				Action         & \begin{tabular}[c]{@{}l@{}}Valence: Domain-wall\\ Sea: staggered\end{tabular} \\ \hline
				Gauge configs  & MILC -- thanks!                                                               \\ \hline
				$m_\pi$        & 130 - 400 MeV                                                                 \\ \hline
				$a$            & 0.06 - 0.15 fm                                                                \\ \hline
				$N_f$          & 2 + 1 + 1                                                                     \\ \hline
				\end{tabular}
			\end{table}
			\begin{figure}
				\includegraphics[width=0.7\textwidth]{./figs/milc_cow.png}
				%\caption*{}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Example fit}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/FKFpi_vs_epi_xpt_nnnlo_FV_PP.pdf}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Model Parameters}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begin{center}
				\begin{tabular}{rccc}
					order& $N_{L_i}$& $N_{\chi}$& $N_{a}$\\
					\hline
					{\color{CornflowerBlue} NLO}     & 1     & 0         & 0\\
					{\color{YellowOrange} NNLO} & 7     & 2         & 2\\
					{\color{YellowGreen} NNNLO}& 0     & 3         & 3\\
					\hline
					Total   & 8     & 5         & 5
				\end{tabular}
			\end{center}
			Many LECs
			\begin{itemize}
				\item constrain with priors
				\item check priors using empirical Bayes
			\end{itemize}	
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/FKFpi_vs_epi_convergence_xpt_nnnlo_FV_PP}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Empirical Bayes Method}


	Let $M = \{\Pi, f\}$ denote a model. Per Bayes's theorem:
	\begin{equation*}
		{\color{RubineRed} p(\Pi | D, f)} 
		= \frac{{\color{ProcessBlue} p(D |\Pi, f)} {\color{JungleGreen} p(\Pi | f)}}{p(D | f)}
	\end{equation*}

	Assuming a uniform distribution for ${\color{JungleGreen} p(\Pi | f)}$:
	\begin{equation*}
		\text{peak of } {\color{ProcessBlue} p(D |\Pi, f)} 
		\implies \text{peak of }  {\color{RubineRed} p(\Pi | D, f)}
	\end{equation*}
	where ${\color{ProcessBlue} p(D |\Pi, f)} $ is the (readily available) likelihood
	\vfill
	\begin{columns}
		\begin{column}{0.4\textwidth}
		\begin{itemize}
			\item Caveat: the uniformity assumption breaks down if we vary too many parameters separately or make our priors too narrow
		\end{itemize}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=\textwidth]{./figs/empirical_bayes_fk_fpi.png}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Model averaging}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{figure}
			\includegraphics[width=\linewidth]{figs/hist_FF.pdf}
			\end{figure}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{align*}
				\begin{array}{rl}
					\times 2:& \text{expanded/ratio}  \\
					\times 2:& \text{N$^2$LO: $\chi$PT or Taylor}\\
					\times 3:& \mu = \Lambda_\chi \in \{ F_\pi, F_K, \sqrt{F_\pi F_K}\} \\
					\times 2:& \text{incl./excl. $\alpha_S$} \\
					\hline
					24:& \text{total choices}
				\end{array}
			\end{align*}
		\end{column}
	\end{columns}

	Weigh fits with Bayes factor
	\begin{itemize}
		\item marginalization $\implies$ can compare models with different parameters
		\item automatically penalizes overcomplicated models
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Model comparison}
	
	\begin{figure}
		\centering
		\begin{subfigure}[b]{.4\linewidth}
		\includegraphics[width=\linewidth]{figs/hist_FF}
		\setcounter{subfigure}{2}%
		\end{subfigure}

		\begin{subfigure}[b]{.4\linewidth}
			\includegraphics[width=\linewidth]{figs/hist_ct}
			\setcounter{subfigure}{0}%
			\end{subfigure}
			\begin{subfigure}[b]{.4\linewidth}
			\includegraphics[width=\linewidth]{figs/hist_ratio}
			\end{subfigure}
	\end{figure}

\end{frame}


\begin{frame}
	\frametitle{Error budget}
	\begin{columns}
		\begin{column}{0.4\textwidth}
			\begingroup \scriptsize
			\begin{align*}
			&F_K / F_\pi &= 1.1964 \pm 0.0044\\
			\cline{3-3}
			&\text{Statistical} & 0.0032 \\ 
			&\text{Disc} & 0.0020 \\
			&\text{Phys Point} & 0.0015 \\
			&\text{Model Unc} & 0.0015 \\
			&\text{Chiral} & 0.0012 \\
			&\text{Volume} & 0.0001  \\
			\cline{1-3}
			&\delta \left(\frac{F_K}{F_\pi} \right)_\text{SU(2)} &= -0.00215 \pm 0.00072
			\end{align*}
			\endgroup
		\end{column}
		
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[height=0.8\textheight,keepaspectratio]{./figs/model_breakdown.pdf}
			\end{figure}
		\end{column}
\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Comparison with previous results}
	\begin{figure}
		\centering
		\includegraphics[width=0.7\textwidth]{figs/collab_comparison.pdf}
	\end{figure}
\end{frame}


\begin{frame}
	\frametitle{$|V_{us}|$ from $F_K / F_\pi$}

	\begin{columns}
		\begin{column}{0.350\textwidth}
		This result:
		\begin{equation*}
			\frac{|V_{us}|}{|V_{ud}|} = 0.2311(10)
		\end{equation*}
		With experiment:
		\begin{equation*}
			|V_{us}| = 0.2251(10)
		\end{equation*}
	\end{column}

		\begin{column}{0.65\textwidth}
			
			\begin{figure}
				\centering
				\includegraphics[width=1.0\textwidth]{figs/VusVud.pdf}
			\end{figure}

		\end{column}

	\end{columns}
	\vfill
	\begin{equation*}
		\boxed{\sum_{q\in\{d, s, b\}} |V_{uq}|^2 = 0.99977(59)}
	\end{equation*}
\end{frame}


\begin{frame}
	\frametitle{$F_K/F_\pi$ results}
	In conclusion:
	\begin{itemize}
		\item $F_K/F_\pi \implies |V_{us}|$
		\item $F_K/F_\pi$ is a \emph{gold-plated} quantity and can be used to test lattice QCD actions
	\end{itemize}
	\vfill
	\begin{equation*}
	\boxed{F_{K^\pm}/F_{\pi^\pm} = 1.1942(45)} \implies 
	\boxed{\sum_{q\in\{d, s, b\}} |V_{uq}|^2 = 0.99977(59)}
	\end{equation*}
\end{frame}

\section{Hyperons on the lattice}
\begin{frame}
	\begin{center}
		\Huge Hyperons on the lattice
	\end{center}	
\end{frame}

\begin{frame}
	\frametitle{Why study hyperons?}

	{\color{ProcessBlue} Hyperon}: a baryon containing at least one strange quark but no heavier quarks
	
	\begin{columns}
	\begin{column}{0.5\textwidth}

	Why study hyperons?
	\begin{itemize}
		\item {\color{JungleGreen} Decays $\implies$ $V_{us}$ $\implies$  
		top-row unitarity: $|V_{ud}|^2 + | V_{us}|^2 +| V_{ub}|^2 \stackrel{?}{=} 1$}
		\item Axial charge, mass spectra important for neutron star equation of state
		\item Test heavy baryon $\chi$PT
	\end{itemize}
	\vspace{\baselineskip}

	Why the lattice?
	\begin{itemize}
		\item Hypernuclear structure harder to study experimentally
		\item Hyperons decay rapidly in the lab ($\tau < 1$ ns)
	\end{itemize}	
	\end{column}
	\begin{column}{0.4\textwidth}
		\begin{figure}
			\includegraphics[width=0.4\textheight]{./figs/baryon_octet.png}
			\includegraphics[width=0.4\textheight]{./figs/baryon_decuplet.png}
			\caption*{[Wikipedia]}
		\end{figure}
	\end{column}
	\end{columns}
\end{frame}




\begin{frame}
	\frametitle{Tension with unitarity}

	\begin{columns}
		\begin{column}{0.4\textwidth}
			Pure lattice:
			\begin{align*}
				& \vert V_u \vert^2 = 0.9813(66) \\
				& \implies \text{2.8 $\sigma$ deviation}
			\end{align*}

			\vspace{\baselineskip}

			$K_{\ell 2}$ ($F_K/F_\pi$) $+$ PDG:
			\begin{align*}
				& \vert V_u \vert^2 = 0.99883(37) \\
				& \implies \text{3.2 $\sigma$ deviation}
			\end{align*}

			\vspace{\baselineskip}

			$K_{\ell 3}$ ($f_{+}(0)$) $+$ PDG:
			\begin{align*}
				& \vert V_{u} \vert^2 = 0.99794(37) \\
				& \implies \text{5.6 $\sigma$ deviation}
			\end{align*}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/vus_vs_vud.png}
				\caption*{[FLAG (2021); \href{https://arxiv.org/abs/2111.09849}{arXiv:2111.09849}]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Tension with unitarity... maybe even worse?}

	\begin{columns}
		\begin{column}{0.4\textwidth}
			Traditional FLAG recommendation:
			\begin{equation*}
				\left\vert \frac{V_{us}}{V_{ud}} \right\vert \frac{F_{K^\pm}}{F_{\pi^\pm}} = 0.2760(4)
			\end{equation*}

			\vspace{\baselineskip}
		
			QCD + QED:
			\begin{equation*}
				\left\vert \frac{V_{us}}{V_{ud}} \right\vert \frac{F_{K}}{F_{\pi}} = 0.2768(4)
			\end{equation*}
		\end{column}
		\begin{column}{0.6\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/vus_vs_vud_em_lat.png}
				\caption*{[FLAG (2021); \href{https://arxiv.org/abs/2111.09849}{arXiv:2111.09849}]}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Previous work}

	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/bmw_hadron_spectrum.png}
			\caption*{[BMW (2009); \href{https://arxiv.org/abs/0906.3599}{arXiv:0906.3599}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/lin_axial.png}
			\caption*{[Savanur \& Lin (2018); \href{https://arxiv.org/abs/1901.00018}{arXiv:1901.00018}]}
		\end{figure}
		\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{$\Xi$ correlator fits}

	\begin{columns}
		\begin{column}{0.55\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/xi_a12m350.pdf}
		\end{figure}
		\end{column}
		\begin{column}{0.45\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/m_xi_m_pi.png}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{Fit strategy: mass formulae}
	Consider the $S=2$ hyperons in the isospin limit
	\begin{columns}
		\begin{column}{0.5\textwidth}
			\begin{align*}
				M_\Xi^{(\chi)} = &\phantom{+}  M_\Xi^{(0)} \\
				&+ {\color{ProcessBlue} \sigma_\Xi} \Lambda_\chi \epsilon_\pi^2 \\
				&- \frac{3\pi}{2} {\color{JungleGreen} g_{\pi\Xi\Xi}^2} \Lambda_{\chi} \epsilon_\pi^3 \\
				&\qquad- {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu) \\
				&+ \frac{3}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \sigma_\Xi} - {\color{ProcessBlue} \overline{\sigma}_\Xi} ) \Lambda_\chi \epsilon_\pi^2 
				\mathcal{J} (\epsilon_\pi, \epsilon_{\Xi\Xi^*}, \mu) \\
				&\qquad + \alpha_\Xi^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi}^{(4)} \Lambda_\chi \epsilon_\pi^4
			\end{align*}
		\end{column}
	\begin{column}{0.5\textwidth}
		\begin{align*}
			M_{\Xi^*}^{(\chi)} = &\phantom{+}  M_{\Xi^*}^{(0)} \\
			&+ {\color{ProcessBlue} \overline{\sigma}_\Xi} \Lambda_\chi \epsilon_\pi^2  \\
			&- \frac{5\pi}{6} {\color{JungleGreen} g_{\pi\Xi^*\Xi^*}^2} \Lambda_{\chi} \epsilon_\pi^3 \\
			&\qquad - \frac{1}{2} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} \Lambda_{\chi} \mathcal{F}(\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu) \\
			&+ \frac{3}{4} {\color{JungleGreen} g_{\pi\Xi^*\Xi}^2} ({\color{ProcessBlue} \overline{\sigma}_\Xi} -{\color{ProcessBlue} \sigma_\Xi} ) \Lambda_\chi \epsilon_\pi^2 
			\mathcal{J} (\epsilon_\pi, -\epsilon_{\Xi\Xi^*}, \mu)  \\
			&\qquad + \alpha_{\Xi^*}^\text{(4)} \Lambda_{\chi} \epsilon_\pi^4 \log{\epsilon_\pi^2} + \beta_{\Xi^*}^{(4)} \Lambda_\chi \epsilon_\pi^4
			\end{align*}
	\end{column}
	\end{columns}
	\vfill
	Some observations:
	\begin{itemize}
		\item Many {\color{ProcessBlue} shared LECs} between expressions $\implies$ fit simultaneously
		\item Mass fits depend on {\color{JungleGreen} axial charges} 
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Hyperon mass spectrum: $\Xi$ preliminary results}
	\begin{columns}
	\begin{column}{0.5\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/m_xi_order_xpt.png}
		\end{figure}
	\end{column}
	\begin{column}{0.5\textwidth}
		\begin{align*}
			\begin{array}{rl}
				+ 1:& \text{Taylor }\mathcal{O}(m^2_\pi) \\
				+ 1:& \text{$\chi$PT } \mathcal{O}(m^3_\pi)\\
				+ 3:& \text{Taylor } \mathcal{O}(m^4_\pi)\oplus \text{$\chi$PT } \left\{ 0, \mathcal{O}(m^3_\pi), \mathcal{O}(m^4_\pi) \right\} \\
				\hline 
				5:& {\color{RubineRed} \text{chiral choices}} \\
				\\
				\times 5:& {\color{RubineRed}  \text{chiral choices}}  \\
				\times 2:& \left\{ \mathcal{O}(a^2), \mathcal{O}(a^4)\right\}\\
				\times 2:& \text{incl./excl. strange mistuning} \\
				\times 2:& \text{na\"ive priors or empirical priors} \\
				\hline
				40:& \text{total choices}
			\end{array}
			\end{align*}
	\end{column}
	\end{columns}
	\vfill
	\begin{equation*}
		\boxed{M_{\Xi} = 1339(17)^\text{s}(02)^\chi(05)^a(00)^\text{phys}(01)^\text{M}(??)^\text{V}}
	\end{equation*}
\end{frame}


\begin{frame}
	\frametitle{Hyperon summary}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		In conclusion:
		\begin{itemize}
			\item Chiral mass and charge expressions share many LECs and would benefit from a simultaneous fit
			\item Hyperon decays provide an alternate method for extracting $|V_{us}|$
			\item Competitive if $O(1\%)$ determination of the form factors
		\end{itemize}
		\vspace{\baselineskip}

		To do:
		\begin{itemize}
			\item Add finite volume effects to mass fits
			\item (Simultaneously) fit axial charges
			\item Calculate hyperon-to-nucleon form factors
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{figs/xi_ga_a15m400.png}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}

\section{The nucleon sigma term}
\begin{frame}
	\begin{center}
		\Huge The nucleon sigma term
	\end{center}	
\end{frame}

\begin{frame}
	\frametitle{The sigma terms: what are they and what are they good for?}
	\begin{columns}
	\begin{column}{0.5\textwidth}
	By definition, the sigma terms are the quark condensates inside the nucleon
	\begin{equation*}
		\sigma_{q} = m_{q}\braket{N | \overline q q | N}
	\end{equation*}

	These parameterize:
	\begin{itemize}
	\item the $q$-quark mass shift to $M_N$
	\item the coupling to the Higgs
	\item {\color{ProcessBlue} the spin-independent coupling to some dark matter candidates}
	\end{itemize}
	\end{column}

	\begin{column}{0.5\textwidth}
	\begin{figure}
		\includegraphics[width=0.8\textwidth]{./figs/lux.jpg}
		\caption*{Large Underground Xenon experiment}
	\end{figure}
	\end{column}
	\end{columns}

\end{frame}


\begin{frame}
	\frametitle{Previous work}
	\begin{columns}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/flag_sigma.png}
			\caption*{[FLAG, 2019; \href{https://arxiv.org/abs/1902.08191}{arXiv:1902.08191}]}
		\end{figure}
		\end{column}
		\begin{column}{0.50\textwidth}
		\begin{figure}
			\includegraphics[width=1.0\textwidth]{./figs/gupta_sigma.png}
			\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
		\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\begin{frame}	
	\frametitle{Expansion of $\sigma_{N\pi}$}
	%Expand $\sigma_{N\pi} = \hat m \frac{\partial M_N}{\partial \hat m} \approx m_\pi^2 \frac{\partial M_N}{\partial m_\pi^2} \approx \frac 12 \epsilon_\pi \frac{\partial M_N}{\partial \epsilon_\pi}$ where $\epsilon_\pi = m_\pi / \Lambda_\chi = m_\pi / 4 \pi F_\pi$ 
	Expand $\sigma_{N\pi} = \hat m \frac{\partial M_N}{\partial \hat m} \rightarrow \hat m \frac{\partial}{\partial \hat m} = \frac 12 \epsilon_\pi (\cdots) \frac{\partial}{\partial \epsilon_\pi}$
	\begin{equation*}
	\sigma_{N\pi} = \frac 12 \epsilon_\pi \left[
		1 
		+ \epsilon_\pi^2 \left( \frac 52 - \frac 12 \overline \ell_3 - 2 \overline \ell_4 \right) 
		+ \mathcal{O} \left(\epsilon_\pi^3\right)
	\right]
	\overbrace{
	\left[
		{\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}}
		+ {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}}
	\right]
	}^{\frac{\partial M_N}{\partial \epsilon_\pi}}
	\end{equation*}

	\begin{align*}
	\frac 12 \epsilon_\pi {\color{ProcessBlue} \Lambda_\chi^* \frac{\partial \left(M_N/\Lambda_\chi\right)}{\partial \epsilon_\pi}} &=
	\frac 12 \Lambda_\chi^* \left[
		\left({\color{RubineRed} -2 c_0 \left(\overline \ell_4  - 1 \right)}  + 2 \beta^\text{(2)}_N \right) \epsilon_\pi^2 
		+ \mathcal{O}\left(\epsilon_\pi^3\right)
	\right] 
	\sim 10 \text{ MeV} 
	\\
	\frac 12 \epsilon_\pi {\color{JungleGreen}\frac{M_N^*}{\Lambda_\chi^*} \frac{\partial \Lambda_\chi}{\partial \epsilon_\pi}} &=
	\frac 12 M_N^* \left[ {\color{RubineRed} 2 \left(\overline \ell_4  - 1 \right)\epsilon_\pi^2 } 
	+ \mathcal{O}\left(\epsilon_\pi^3\right) \right]
	\sim 40 \text{ MeV}
	\end{align*}
	\begin{itemize}
		\item Fitting {\color{ProcessBlue} $M_N/\Lambda_\chi	$} requires an {\color{JungleGreen} extra term}
		\item Largest contribution comes from {\color{JungleGreen} second term} $\implies$ $\overline \ell_4$ must be precisely determined
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Comparing $\chi$PT terms by order}

	\begin{figure}
		\includegraphics[width=1.0\textwidth]{./figs/histogram_all.pdf}
		%\caption*{[Gupta, 2021; \href{https://arxiv.org/abs/2105.12095}{arXiv:2105.12095}]}
	\end{figure}
	Here we use:
	\begin{itemize}
		\item $F_\pi$-derived $\chi$PT terms up to $\mathcal{O}(\epsilon_\pi^2)$ \& $M_N$-derived $\chi$PT terms up to $\mathcal{O}(\epsilon_\pi^4)$ 
		\item FLAG average for $\overline \ell_4$
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Sigma term summary}

	\begin{columns}
		\begin{column}{0.5\textwidth}
		In conclusion:
		\begin{itemize}
			\item Tension exists between phenomenology and the lattice w.r.t. $\sigma_{N\pi}$
			\item Can extract $\sigma_{N\pi}$ from a dimensionless fit of $M_N/\Lambda_\chi$
			\item However, this requires a precise determination of the LECs associated with the chiral expression for $F_\pi$
		\end{itemize}
		\vspace{\baselineskip}

		To do:
		\begin{itemize}
			\item Carefully determine $F_\pi$ LECs for $\sigma_{N\pi}$
			\item Add FV corrections
		\end{itemize}
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=1.0\textwidth]{./figs/histogram_sigma.pdf}
				%\caption*{Some fig}
			\end{figure}
		\end{column}
	\end{columns}
\end{frame}


\section{Extra slides}
\begin{frame}
	\begin{center}
		\Huge Extra slides
	\end{center}	
\end{frame}


\begin{frame}
	\frametitle{Approximate QCD symmetry: chirality}
	 Consider massless quarks
	\begin{align*}
		&L_{\text{QCD}} = \overline{\psi} (i \gamma^\mu D_\mu - m) \psi - \frac 14 G^a_{\mu \nu} G_a^{\mu \nu} \\
		&\rightarrow L_0 = \overline{\psi}_L (i \gamma^\mu D_\mu) \psi_L + \overline{\psi}_R (i \gamma^\mu D_\mu) \psi_R - \frac 14 G^a_{\mu \nu} G_a^{\mu \nu}
	\end{align*}
	\begin{itemize}
		\item Spontaneously broken: $\braket{0 | \overline{\psi} \psi | 0} = \braket{0 | \overline{\psi}_L \psi_R + \overline{\psi}_R \psi_L| 0} \ne 0$
		$\implies \text{Goldstone bosons}$
		\item Explicitly broken: $L = \overline{\psi} (i \gamma^\mu D_\mu - {\color{RubineRed}\overbrace{m}^{\ne 0}}) \psi - \frac 14 G^a_{\mu \nu} G_a^{\mu \nu}$
		$\implies \text{pseudo-Goldstone bosons}$ 
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Fit strategy: $M_N \rightarrow M_N/\Lambda_\chi$}
	Instead of fitting $M_N$, fit dimensionless $M_N/\Lambda_\chi$ $\qquad (\Lambda_\chi = 4 \pi F_\pi\,, \,\,\epsilon_\pi = m_\pi / \Lambda_\chi)$
	\begin{align*}
		\frac{M_N}{{\color{JungleGreen}4 \pi F_\pi}} = &\phantom{+}  c_0
		& \text{(LLO)}\\
		&+ \left(\beta^\text{(2)}_N - {\color{JungleGreen} c_0 \overline \ell_4^r} \right) \epsilon_\pi^2 
		+ {\color{JungleGreen} c_0 \epsilon_\pi^2 \log \epsilon_\pi^2} 
		& \text{(LO)} \nonumber \\
		&- \frac{3\pi}{2} g_{\pi NN}^2  \epsilon_\pi^3
		& \text{(NLO)} \nonumber\\
		& + \left( \beta_{N}^{(4)} + {\color{JungleGreen} c_0 \left(\overline \ell_4^r\right)^2} - {\color{JungleGreen} c_0 \beta_{F}^{(4)}}  \right) \epsilon_\pi^4
		&\text{(N$^2$LO)} \\
		&\qquad - {\color{JungleGreen}\frac 14 c_0 \epsilon_\pi^4  \left(\log  \epsilon^2 \right)^2} 
		+ \left( \alpha_N^\text{(4)} - {\color{JungleGreen} c_0 \alpha_F^\text{(4)}} - {\color{JungleGreen} 2 c_0 \overline \ell_4^r} \right) \epsilon_\pi^4 \log{\epsilon_\pi^2} 
		\nonumber
	  \end{align*}
	%Some observations:
	\begin{itemize}
		\item The {\color{JungleGreen} $1/4 \pi F_\pi$} expansion doesn't \emph{require} fitting additional LECs; it only adds some log terms
		\item We'd like to push this $M_N/\Lambda_\chi$ analysis as far as possible
	\end{itemize}
\end{frame}


\begin{frame}
	\frametitle{Ideal properties of a lattice action}
	Discretize fermion action ($S=S_F + S_G$):
	\begin{equation*}
		S_F^\text{QCD} = \int d^4x \, \overline \psi \left( i \gamma^\mu D_\mu -m \right) \psi 
		\quad \rightarrow \quad S_F^\text{LQCD} = a^4 \sum_{n,m \in \Lambda} \overline{\psi}(n) {\color{ProcessBlue} \underbrace{D(n | m)}_\text{Dirac matrix}} \psi(m)
	\end{equation*}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			Ideally, the discretized action should:
			\begin{itemize}
				\item act locally
				\item have translation invariance
				\item respect chiral symmetry
				\item have no fermion doublers
			\end{itemize}
			\vfill
		\end{column}
		\begin{column}{0.5\textwidth}
			\begin{figure}
				\includegraphics[width=0.9\textwidth]{figs/fermion_propagator.pdf}
				\caption*{[Fodor \& Hoelbling (2012); \href{https://arxiv.org/abs/1203.4789}{arXiv:1203.4789}]}
			\end{figure}
		\end{column}
	\end{columns}
	
	{\color{RubineRed}\emph{Nielsen-Ninomiya Theorem}}: Impossible for (discretized) Dirac matrix to simultaneously satisfy all four desiderata in an {\color{JungleGreen} \emph{even}} dimensional theory
\end{frame}


\begin{frame}
	\frametitle{Comparison of lattice actions}
	\begin{columns}
		\begin{column}{0.7\textwidth}
		\begin{table}[]
			\begin{tabular}{l|lll|l}
				\begin{tabular}[c]{@{}l@{}}Fermion\\ action\end{tabular} & Doublers & Local & \begin{tabular}[c]{@{}l@{}}Chiral\\ symmetry\end{tabular} & Cost \\ \hline
				Naive & {\color{RubineRed} Yes (16)}  & Yes & {\color{RubineRed} No} & Cheap \\
				Wilson-Clover & No & Yes  & {\color{RubineRed} No} & Cheap \\
				{\color{ProcessBlue} Staggered} & {\color{RubineRed} Yes (4)}   & {\color{RubineRed} No}  & {\color{Orchid} Some} & Cheap \\
				{\color{JungleGreen} Domain Wall} & No & Yes & Yes & Expensive \\
				Overlap & No & {\color{RubineRed} No} & Yes & Expensive 
			\end{tabular}
		\end{table}
		\end{column}
		\begin{column}{0.3\textwidth}
			\includegraphics[width=0.9\textwidth]{figs/hadron_innards.png}
		\end{column}
	\end{columns}

	Our action:
	\begin{columns}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
		\item {\color{ProcessBlue} sea quarks}: staggered
		\item {\color{JungleGreen} valence quarks}: domain-wall
	\end{itemize}	
	\end{column}
	\begin{column}{0.5\textwidth}
	\begin{itemize}
		\item $N_f = 2 + 1 + 1$
		\item $\mathcal{O}(a^2)$ discretization errors
	\end{itemize}	
	\end{column}
	\end{columns}
\end{frame}


\begin{frame}
	\frametitle{M\"obius domain wall fermions}
	\begin{itemize}
		\item Introduce fifth dimension $\implies$ circumvent Nielsen-Ninomiya
		\item M\"obius: DW variant with more supression of residual symmetry breaking $\implies$ fewer lattice sites needed
	\end{itemize}
	\begin{figure}
		\includegraphics[width=0.7\textwidth]{figs/domain_wall.png}
		\caption*{[LHPC (2006)]}
	\end{figure}

\end{frame}

\begin{frame}
	\frametitle{Transition matrix element for $B_1 \rightarrow B_2 + l^- + \overline{\nu}_l$}

	\begin{equation*}
		T_\text{fi} = \frac{G_\text{F}}{\sqrt{2}} V_{us} 
		\left[
		\overbrace{\braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}^\text{\color{ProcessBlue} axial-vector} 
		- \overbrace{\braket{B_2 | \overline u \gamma_\mu s | B_1}}^\text{\color{JungleGreen} vector} 
		\right]
		\overline l \gamma^\mu (1 - \gamma^5) \nu_l 
	\end{equation*}

	with hadronic matrix elements

	\begin{align*}
		{\color{ProcessBlue} \braket{B_2 | \overline u \gamma_\mu \gamma^5 s | B_1}}
		&= g_A(q^2) \gamma_\mu \gamma^5 
		+ \underbrace{\cancel{\frac{f_\text{T}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu \gamma^5}}_\text{\color{RubineRed} G-parity} + \frac{f_\text{P}(q^2)}{2 M} q_\mu \gamma^5 \\
		{\color{JungleGreen} \braket{B_2 | \overline u \gamma_\mu s | B_1}}
		&= g_V(q^2) \gamma_\mu + \frac{f_\text{M}(q^2)}{2 M} i \sigma_{\mu \nu} q^\nu 
		+ \overbrace{\cancel{\frac{f_\text{S}(q^2)}{2 M} q_\mu}}^\text{\color{RubineRed} CVC}
	\end{align*}

\end{frame}

\end{document}
